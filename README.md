# Software Studio 2018 Spring Lab03 Bootstrap and RWD
![Bootstrap logo](./Bootstrap logo.jpg)
## Grading Policy
* **Deadline: 2018/03/20 17:20 (commit time)**

## Todo
1. Check your username is Student ID
2. **Fork this repo to your account, remove fork relationship and change project visibility to public**
3. Make a personal webpage that has RWD
4. Use bootstrap from [CDN](https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css)
5. Use any template form the inernet but you should provide the URL in your project README.md
6. Use at least **Ten** bootstrap elements by yourself which is not include the template
7. You web page template should totally fit to your original personal web page content
8. Modify your markdown properly (check all todo and fill all item)
9. Deploy to your pages (https://[username].gitlab.io/Lab_03_Bootstrap)
10. Go home!

## Student ID , Name and Template URL
- [ ] Student ID : 105000096
- [ ] Name :Yang Rui
- [ ] URL :http://www.free-css.com/free-css-templates/page218/minifolio

## 10 Bootstrap elements (list in here)
1. https://www.w3schools.com/cssref/pr_background-image.asp
2. https://www.w3schools.com/cssref/css3_pr_text-shadow.asp
3. https://www.w3schools.com/cssref/pr_border-bottom_color.asp
4. https://www.w3schools.com/cssref/pr_outline-color.asp
5. https://www.w3schools.com/cssref/pr_outline-style.asp
6. https://www.w3schools.com/cssref/pr_text_color.asp
7. https://www.w3schools.com/cssref/css3_pr_column-rule-color.asp
8. https://www.w3schools.com/cssref/css3_pr_column-gap.asp
9. https://www.w3schools.com/cssref/css3_pr_column-rule-style.asp
10. https://www.w3schools.com/cssref/css3_pr_column-count.asp
